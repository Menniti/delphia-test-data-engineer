# Delphia Test - Luiz Menniti

## Python version - 3.10.4

## Exercise 1

### To run the code for count all words from tweets.tar

Download data
```
aws s3 cp --no-sign-request s3://delphia-assignment/twitter/tweets.tar tweets.tar
```

Script arguments
```
arguments: 
 extract - <str> - Yes/No - It will decide if you want to extrat tweets.tar
 extract_path_name - <str> - Pathname (tweets.rar) - Choose the path to extrar .rar file
 chunk_size - <int> - Is the number of bytes to load in memory for each bulk of lines read from files 
 count_by_user - <str> - Yes/No - Choose if want to count_by_user or not. In case of No, it will run count every word
 root_folder - <str> - Is the name of root_folder where files to be read are located <2017_01_01>
 start_date - <str> - Start date to filter data when count_by_user = Yes. Format mm/dd/YYYY
 end_date - <str> - End date to filter data when count_by_user = Yes. Format mm/dd/YYYY
```
Run the script
```
sample - exercise 1
python main.py No None 10000 No 2017_01_01

sample - exercise 2
python main.py No None 10000 Yes 2017_01_01 01/01/2017 02/01/2017
```

## Question 1
### 1. How would you approach the second exercise if the the data was 30GB? 10TB?
#### The approach will be relatively near to what we have now, open data in chunks to avoid memory overflow, setup args to execute from any scheduler/docker, provide queue structure to share data between processes etc.
#### Things to add in the future are: Add map/reduce/filter functions to provide lazy computation and return iterators and iterate only when the data is needed and use some cluster structure such Spark, Ray, Dask to provider power computing in order to parallelize the calculations, opening files and write files. We can consider materializing the data or even use powerful databases such as Elastic search to deal with those word aggregations.


### 2. What are the trade-offs of your approach(es) in the previous question?
#### Adding map/reduce/filter will add lazy computation, which brings control in an abstraction data (interators) and uses RAM/CPU only when it is needed. Using Cluster for parallel processing, brings the possibility to process 10 TB within a few seconds, but is not for free, it needs a lot of ram available to handle it. Also it is needed to manage, log that data from then, observe the systems/machines that operate it. In this case it is recommended to use a managed service to avoid spending more time setup and cluster systems than actually build the business logic.
#### A solution using cluster databases such as Elastic search, is very powerful, it has a very fast aggregator, api rest to put / get data and also has the Kibana to visualize and log stash/beats family to bring metrics from each part of the cluster (including other applications). It also needs to be managed so it is recommended to get some services.
