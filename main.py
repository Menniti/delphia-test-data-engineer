
import tarfile
import json    
import sys
import bz2
import os
import string
import re
import multiprocessing as mp
import datetime

def count_words_from_text(text_arr):
    '''Count all words from list and return a dict of each word, with amount of times it appears
    
    Parameters:
    text_arr (str): text_arr list of texts

    Returns:
    content_count (dict): List of each word, without ponctuation and spaces

    TODO write unit_test
    '''
    content_count = {}
    for text in text_arr:
        if text in content_count:
            content_count[text] = content_count[text] + 1
        else:
            content_count[text] = 1
    return content_count

def flow_words(text):
    '''Find all words coming from text, and remove ponctuation and spaces
    
    Parameters:
    text (str): Text coming from

    Returns:
    text_arr (list): List of each word, without ponctuation and spaces

    TODO write unit_test
    '''
    text_trans = text.translate(str.maketrans('', '', string.punctuation))
    text_arr = re.findall(r'\w+|[^\s\w]+', text_trans)
    return text_arr

def merge_count_files(q):
    '''Merge the count files from user, also reads data from Queue
    
    Parameters:
    q (Queue): Queue Object

    Returns:
    merge_count (dict): Merge the count of each file

    TODO write unit_test
    '''
    print("Starting merge count files....")
    merge_count = {}
    while True:
        if q.empty() == True:
            break                     
        contents = q.get()
        for content in contents:
            for key, value in content.items():
                if key in merge_count:
                    merge_count[key] = merge_count[key] + value
                else:
                    merge_count[key] = value
    print("Merge has ended sucessfully!")
    return merge_count

def merge_count_files_by_user(q):
    '''Merge the count files by user, also reads data from Queue
    
    Parameters:
    q (Queue): Queue Object

    Returns:
    merge_count (dict): Merge the count of each file by user

    TODO write unit_test
    '''
    print("Starting merge count files....")
    merge_count = {}
    while True:
        if q.empty() == True:
            break                     
        contents = q.get()
        for content in contents:
            for key, json_result in content.items():
                for json_key, json_value in json_result.items():
                    composite_key = "{}#{}".format(key,json_key)
                    if composite_key in merge_count:
                        merge_count[composite_key] = merge_count[composite_key] + json_value
                    else:
                        merge_count[composite_key] = json_value
    print("Merge has ended sucessfully!")
    return merge_count
                

def get_paths(root_folder):
    '''Get all files, from rootfolder, and provide a array of path of each file present on folder
    
    Parameters:
    root_folder (str): Folder that is looking to read files inside

    Returns:
    paths (list): List of all paths of each file inside of folder

    TODO write unit_test
    '''
    dirs_topdown = []
    files_topdown_arr = []
    paths = []
    count = 0
    for _, dirs, files in os.walk(root_folder,topdown=True):
        if dirs != []:
            dirs_topdown = dirs
        if files != []:
            files_topdown_arr.append(files)

    for directory in dirs_topdown:
        files_arr = files_topdown_arr[count]
        for files_name in files_arr:
            root = root_folder
            path = "{}/{}/{}".format(root,directory,files_name)
            paths.append(path)
        count = count + 1
    return paths

def write_file(file_name,result):
    '''Get all files, from rootfolder, and provide a array of path of each file present on folder
    
    Parameters:
    file_name (str): name of file to be written
    result (list<dict>): List of dicts to be written

    Returns:
    paths (list): List of all paths of each file inside of folder

    TODO write unit_test
    '''
    with open(file_name, 'w', encoding='utf-8') as f:
        for key, value in result.items():
            json_line = {}
            json_line[key] = value
            f.write(json.dumps(json_line))
            f.write('\n')
        f.close()

def read_files(paths, queue_obj, chunk_size, countByUser,start_date_int=None, end_date_int=None):
    '''Read files from .json.bz2, and put data to Queue object
    
    Parameters:
    paths (list): List of all files
    queue_obj (Queue): Queue Object
    chunk_size (int): The amount of data to be read to avoid memory overflow
    countByUser (str): Yes/No to check what type of count is required

    ***contents (dict): This function put data to Queue***
    
    TODO write unit_test
    '''
    contents = []
    
    for path in paths:
        with bz2.open(path, mode='r') as f:
            while True:
                # Open in chunks to avoid buffer overflow
                json_clean = {}
                line_bytes = f.readlines(chunk_size)
                # Get out of while if there is no more data to read on that file
                if not line_bytes:
                    print("{} read sucessfully!".format(path))
                    break
                for line in line_bytes:
                    result_json = json.loads(line.decode('utf8').strip('\n'))
                    if "text" not in result_json:
                        continue
                    if countByUser == "Yes":
                        #TODO review timestamp filter
                        timestamp_int = int(result_json["timestamp_ms"])
                        if  timestamp_int <= start_date_int and timestamp_int >= end_date_int:
                            continue
                        flow_text_arr = flow_words(result_json["text"])
                        content_result = count_words_from_text(flow_text_arr)
                        json_clean[result_json["user"]["id_str"]] = content_result
                        contents.append(json_clean)
                    else: 
                        flow_text_arr = flow_words(result_json["text"])
                        content_result = count_words_from_text(flow_text_arr)
                        contents.append(content_result)
                queue_obj.put(contents)
                contents = []


if __name__ == "__main__":
    
    print(
        "Hello count words"
    )

    ##Arguments - to make easy to execute in pipelines/docker/etc.
    extract = sys.argv[1]
    extract_data_path = sys.argv[2]
    chunk_size = int(sys.argv[3])
    countByUser = sys.argv[4]
    root_folder = sys.argv[5]
    file_name = ''
    if countByUser == "Yes":
        start_date_int = datetime.datetime.strptime(sys.argv[6], "%d/%m/%Y").timestamp()
        end_date_int = datetime.datetime.strptime(sys.argv[7], "%d/%m/%Y").timestamp()
        file_name = "countsByUser.txt"
    else:
        file_name = "counts.txt"
    
    ##Extract data from 
    if extract == "Yes":
        tweets = tarfile.open(name=extract_data_path ,mode='r', fileobj=None, bufsize=chunk_size)
        tweets.extractall()
    
    ##Get all path files
    paths = get_paths(root_folder)
    
    #TODO FINISH the implementation of multiprocessing
    q = mp.Queue()
    read_files(paths,q,chunk_size,countByUser)
    result = {}

    if countByUser == "Yes":
        result = merge_count_files_by_user(q)
    else:
        result = merge_count_files(q)

    write_file(file_name,result)

    exit()